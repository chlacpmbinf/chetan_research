#!/usr/bin/env python
# coding: utf-8

# # GTEX Exploratory Data Analysis

# In[131]:


import seaborn as sns
import pandas as pd
import warnings
warnings.filterwarnings('ignore')
from matplotlib import pyplot as plt
from scipy.stats import mannwhitneyu,ranksums
import os
import statsmodels.stats.multitest as st


# In[135]:


with open('cancer_genes.txt') as f:
    cancer_genes = f.read().splitlines()


# In[136]:


mutations = pd.read_csv('GTEx_Analysis_2017-06-05_v8_RNA_MuTect_Yizhak19_RNA_MuTect_GTEx_V7_calls_full_table.txt',sep='\t')
relevant_mutations = ['Missense_Mutation','Nonsense_Mutation','Splice_Site', 'Nonstop_Mutation','Start_Codon_SNP']
mutations = mutations[mutations['Variant Classification'].isin(relevant_mutations)]


# ##### Up to this point, created seperate dataframes for skin and esophagus that contain "relevant" mutations. Going further, I can subset by gene.

# In[137]:


def gene_specific_mutations(gene,mut_data):
    return mut_data[mut_data['Hugo Symbol'] == gene]


# In[138]:


def create_plottable_df(dfs):
    dfs[0] = dfs[0].iloc[:,18:-3].dropna()
    dfs[0]['Group'] = 'Mutation in Cancer Gene'
    plot_df = dfs[0]
    other_groups = ['No Cancer Gene Mutations','No Mutations']

    for i in range(1,3):
        if(i!=2):
            dfs[i] = dfs[i].iloc[:,18:-3].dropna()

        else:
            dfs[i] = dfs[i].iloc[:,:-4].dropna()

        dfs[i]['Group'] = other_groups[i-1]
        plot_df = plot_df.append(dfs[i],ignore_index=True) 
    return plot_df


# In[139]:


def create_gs_dfs(cancer_data,merged_data,xcell_data):
    gene_dfs = []
    
    #first group
    gene_dfs.append(cancer_data)

    #second group
    samples_mutations_cancer = list(cancer_data['Sample Name'].unique())
    samples_mutations_nocancer = merged_data[-merged_data['Sample Name'].isin(samples_mutations_cancer)]
    gene_dfs.append(samples_mutations_nocancer)

    #third group
    samples_mutations = list(merged_data['Sample Name'].unique())
    samples_xcell = list(xcell_data['Sample Name'].unique())
    samples_no_mut = list(set(samples_xcell)-set(samples_mutations))
    gene_dfs.append(xcell_data[xcell_data['Sample Name'].isin(samples_no_mut)])
    
    return create_plottable_df(gene_dfs)


# ##### At this point, we have lists of dataframes that contain a dataframe for each group we plan to plot. Now we just need to make the appropriate boxplots.

# In[140]:


def create_boxplots(df,tissue):
    
    fig, axes = plt.subplots(32, 2, figsize=(18, 200))

    fig.suptitle('Mutations in in {0} Tissue'.format(tissue))
    fig.tight_layout(pad=10.0)

    cell_types = ['aDC','Adipocytes','Astrocytes','B-cells','Basophils','CD4+ memory T-cells','CD4+ naive T-cells','CD4+ T-cells','CD4+ Tcm', 'CD4+ Tem','CD8+ naive T-cells','CD8+ T-cells','CD8+ Tcm','CD8+ Tem','cDC',
                  'Chondrocytes','Class-switched memory B-cells','CLP', 'CMP', 'DC', 'Endothelial cells', 'Eosinophils', 'Epithelial cells', 'Erythrocytes', 'Fibroblasts', 'GMP', 'Hepatocytes', 'HSC', 
                  'iDC', 'Keratinocytes', 'ly Endothelial cells', 'Macrophages', 'Macrophages M1', 'Macrophages M2', 'Mast cells', 'Megakaryocytes', 'Melanocytes', 'Memory B-cells', 'MEP', 'Mesangial cells', 'Monocytes', 
                  'MPP', 'MSC', 'mv Endothelial cells', 'Myocytes', 'naive B-cells', 'Neurons', 'Neutrophils', 'NK cells', 'NKT', 'Osteoblast', 'pDC', 'Pericytes', 'Plasma cells', 'Platelets', 'Preadipocytes', 'pro B-cells', 
                  'Sebocytes', 'Skeletal muscle', 'Smooth muscle', 'Tgd cells', 'Th1 cells', 'Th2 cells', 'Tregs']

    counter=0
    for x in range(0,32):
        for y in range(0,2):
            axes[x,y].set_xticklabels(axes[x,y].get_xticklabels(), rotation=40, ha="right")
            sns.boxplot(ax=axes[x,y],data=df,x='Group',y=cell_types[counter])
            sns.swarmplot(ax=axes[x,y],data=df,x='Group',y=cell_types[counter],color="0.25")
            counter = counter+1
            
    
    plt.savefig('{0}.pdf'.format(tissue)) 
    


# In[141]:


def process_tissue_type(tissue):
    tissue_mutations = mutations[(mutations['Tissue'] == tissue)]
    tissue_mutations['Sample Name'] = tissue_mutations['Sample Name'].apply(lambda x:x.replace('-','.'))
    tissue_mutations.fillna(0,inplace=True)
    
    os.chdir('xcell_data')
    xcell_doc_name = 'XCell_'+tissue+'.txt'
    xcell = pd.read_csv(xcell_doc_name,sep='\t')
    os.chdir('../')
    xcell.rename(columns={"SAMP":"Sample Name"},inplace=True)
    
    merged = pd.merge(tissue_mutations,xcell,on='Sample Name',how='left')
    merged = merged.dropna()
    cancer = merged[merged['Hugo Symbol'].isin(cancer_genes)]
    
    tissue_df = create_gs_dfs(cancer,merged,xcell)
    os.chdir('v4_boxplots')
    create_boxplots(tissue_df,tissue)
    os.chdir('../')
    


# In[142]:


tissue_types = list(mutations['Tissue'].unique())


# In[148]:


missed_types = ['Blood_Vessel','Adipose_Tissue','Adrenal_Gland','Salivary_Gland','Small_Intestine','Cervix_Uteri']


# In[143]:


for tissue_type in missed_types:
    if(os.path.exists('xcell_data/XCell_'+tissue_type+'.txt')):
        process_tissue_type(tissue_type)


# In[ ]:




