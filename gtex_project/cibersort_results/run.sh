mkdir -p logs
for i in `ls *runtime_log.txt`
do
TISSUE=`less ${i}|grep mixture|awk -F'/' '{print $NF}'|sed 's/.txt//g'`
RESULTS=`echo ${i}|sed 's/_runtime_log.txt/_Results.txt/g'`
NEW=`echo ${RESULTS}|sed "s/_Results.txt/_${TISSUE}_Results.txt/g"| sed 's/_cibersort//g'`

echo "mv ${RESULTS} ${NEW}"
echo "mv ${i} logs/"

done
