#!/bin/bash

sample_name=$(echo $1 | cut -d/ -f8 | cut -d- -f1-4 | sed 's/.bam//')

/gpfs/fs1/home/cpmuser/bin/depth.06.25 -r /mmfs1/research/cmunugala/beds/nuclear.bed -b $1 -a AVERAGE | grep -v 'X' | grep -v 'Y' > coverage_files/${sample_name}.txt 

/gpfs/fs1/home/cpmuser/bin/depth.06.25 -r /mmfs1/research/cmunugala/beds/MT_0_16569-raw-regions.bed -b $1 -a AVERAGE >> coverage_files/${sample_name}.txt
