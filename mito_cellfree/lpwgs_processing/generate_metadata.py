import argparse
import os



parser = argparse.ArgumentParser()
parser.add_argument('--indir', type=str, required=True)
parser.add_argument('--batch', type=str, required=True)
args = parser.parse_args()

outfile=args.indir+"/"+args.batch+".csv"

fo = open(outfile, "w")
fo.write("batch,sample,uid,postfix\n")
for path in os.listdir(args.indir):
	postfix = path[-21:]
	if os.path.isfile(os.path.join(args.indir, path)) and '_R1_001.fastq.gz' in path and "Undetermined" not in path:
       	#	print(path)
        	sample=path.replace(postfix,"")
        	fo.write(args.batch+","+sample+","+"lp"+sample+","+postfix+"\n")
fo.close()


