import pysam
import pandas as pd
import json
import random
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("bam", help="bam file")
args = parser.parse_args()


chromosomes = [str(x) for x in list(range(1,23))] + ['X','Y','MT']


sample_name = '-'.join(args.bam.split('/')[7].split('-')[0:4]).split('.')[0]
print(sample_name)

