#!/usr/bin/env python
# coding: utf-8

# In[11]:


import pysam
import pandas as pd
import json
import random
import argparse


# In[ ]:


parser = argparse.ArgumentParser()
parser.add_argument("bam", help="bam file")
args = parser.parse_args()


# In[ ]:


#chromosomes = [str(x) for x in list(range(1,23))] + ['X','Y','MT']
chromosomes = ['MT']

# In[2]:


sample_name = '-'.join(args.bam.split('/')[7].split('-')[0:4]).split('.')[0]
samfile = pysam.AlignmentFile(args.bam,'rb')

frag_lens = {}
ratios = {}

for chrom in chromosomes:
    lengths = []
    short_count = 0
    long_count = 0

    for read in samfile.fetch(chrom):
        temp_length = abs(read.template_length)
        if(temp_length != 0 and temp_length < 500):
            lengths.append(temp_length)
        if(temp_length >= 120 and temp_length < 500):
            long_count += 1
        else:
            if(temp_length != 0 and temp_length < 120):
                short_count += 1

    if(long_count > 0):
        ratio = short_count/long_count
        ratios[chrom] = ratio
    else:
        ratios[chrom] = 'undefined'

    if len(lengths) >= 500:
        sampled_lengths = random.sample(lengths,500)
    else:
        sampled_lengths = lengths

    if chrom in frag_lens.keys():
        frag_lens[chrom].append(sampled_lengths)
    else:
        frag_lens[chrom] = sampled_lengths

with open('fragsize_jsons/'+ sample_name+'_fragsizes.json', 'w') as fp:
    json.dump(frag_lens, fp)

output_df = pd.DataFrame(ratios,index=[sample_name])
output_df.to_csv('ratio_files/' + sample_name+'.txt',sep='\t',index=True,header=True)

