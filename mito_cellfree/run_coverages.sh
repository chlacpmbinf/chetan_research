#!/bin/bash

for bam in $(cat /mmfs1/research/cmunugala/mito/mito_cellfree/cpm_ah_bams.txt);
do
	bash calc_coverages.sh $bam

done
