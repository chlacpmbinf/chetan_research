This folder contains all the research work that I did related to mitochondrial analysis from cell free DNA. 

There are three key folders:

	1) ratio files
	2) fragsize jsons
	3) coverage files

These three folders contain files from which analysis can be done on coverage and fragment size. The rest is analysis that I did but it didn't really lead anywhere promising. 
