#!/bin/bash

for short_name in $(head composite_samples.txt | cut -d- -f1)
do
	cna_file=$(ls ../venkatas_outputs/cna_calls/${short_name}*cna_calls.txt | head -1)
	echo $cna_file

done
