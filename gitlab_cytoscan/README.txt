This directory contains files and analysis from the oncokids/oncoscan pan-cancer analysis project. 

In the data folder, you can find oncoscan data along with sequencing data. The bams and vcfs are located at /gpfs/fs1/home/cpmuser/yellapan/scratch/dragen_oncokids. There are vcfs inside dragen_rerun/final_pass/ as well as strelka.




In the ASCAT folder, you can find all ASCAT related files. Under the 'rerun_files' directory, you can find all the samples that we reran ASCAT for after adjusting purity and ploidy values. 





In the final_analysis folder, we see five folders: cDriver, mutational_timing, and composite_analysis, mapping_files, and mafs.
	
	- Composite Analysis contains composite mutation analysis, genomic instability analysis, TP53 mutation analysis. All of these analyses were done inside the jupyter notebook titled 'cleaned_composite_analysis.ipynb'. 

	- Mutational Timing contains 'bradley_terry.R', which contains the code for creating the Bradley Terry model to infer mutational timing. This folder also contains 'sgz.R', which runs SGZ, but we did not end up using sgz in the end. 

	- cDriver contains the files and script to run cDriver, which allows us to determine cancer cell fraction. Cancer cell fraction is used as a metric in the Bradley Terry Model. 

	- Mafs contains all the different version of the mafs that I used during my analysis. 

	- Mapping files contains different files that help map between different sample ID versions. 




The unknown folder contains extra files that I was uncertain about. Perhaps they will be required in the future, so I kept them here. But I am not really sure when/why I used them. 

 
